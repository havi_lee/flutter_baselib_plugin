import 'package:flutter_baselib/flutter_baselib.dart';

import '../bean_factory.dart';

///@date:  2021/10/8 11:19
///@author:  lixu
///@description: 聚合接口JSON解析工具类
///响应数据格式
//{
// 	"reason":"success",
// 	"result":{
// 		"stat":"1",
// 		"data":[
// 			{
// 				"uniquekey":"0ab294fbb2a17e7b143b636c016144a2",
// 				"title":"国庆出游娃丢了，幸亏有他们！",
// 				"date":"2021-10-08 10:55:00",
// 				"category":"头条",
// 				"author_name":"人民资讯",
// 				"url":"https:\/\/mini.eastday.com\/mobile\/211008105526430921781.html",
// 				"thumbnail_pic_s":"https:\/\/dfzximg02.dftoutiao.com\/news\/20211008\/20211008105526_5f97e4c830af3f170c4ede78c81f5c9a_1_mwpm_03201609.jpeg",
// 				"thumbnail_pic_s02":"https:\/\/dfzximg02.dftoutiao.com\/news\/20211008\/20211008105526_5f97e4c830af3f170c4ede78c81f5c9a_2_mwpm_03201609.jpeg",
// 				"thumbnail_pic_s03":"https:\/\/dfzximg02.dftoutiao.com\/news\/20211008\/20211008105526_5f97e4c830af3f170c4ede78c81f5c9a_3_mwpm_03201609.jpeg",
// 				"is_content":"1"
// 			}
// 		],
// 		"page":"1",
// 		"pageSize":"1"
// 	},
// 	"error_code":0
// }
class JuheJsonUtils {
  static String _tag = 'JuheJsonUtils';

  ///聚合接口json解析
  static Future<HttpResultBean<T>> juheApiParseJson<T>(String url, Map<String, dynamic> jsonData, bool isRespListData) async {
    HttpResultBean<T> resultBean = HttpResultBean();
    resultBean.isRespListData = isRespListData;

    try {
      resultBean.code = jsonData['error_code']?.toString() ?? HttpCode.defaultCode;
      resultBean.message = jsonData['reason'];

      if (isRespListData) {
        ///响应的是list
        resultBean.dataList = await _parseToListObject(url, jsonData, resultBean.code);
      } else {
        ///响应的是object
        resultBean.data = await _parseToObject(url, jsonData, resultBean.code);
      }
    } catch (exception) {
      LogUtils.e(_tag, exception.toString());

      if (resultBean.code == HttpCode.defaultCode || HttpCode.successCodeList.contains(resultBean.code)) {
        if (HttpCode.successCodeList.contains(resultBean.code)) {
          resultBean.message = exception.toString();
        }
        resultBean.code = HttpCode.jsonParseException;
      }
    }

    return resultBean;
  }

  ///json解析为list
  ///[jsonData] json数据
  ///[code] http响应码
  static Future<List<T>> _parseToListObject<T>(String url, Map<String, dynamic>? jsonData, String code) async {
    var body = jsonData?['result'];
    var bodyData = body?['data'] ?? body?['list'];

    List<T> listData = [];

    if (HttpCode.successCodeList.contains(code) && bodyData == null) {
      ///请求成功,响应无data,不用解析
    } else {
      if (HttpCode.successCodeList.contains(code) && bodyData != null && bodyData is List) {
        bodyData.forEach((value) async {
          T? bean = BeanFactory.parseObject<T>(value);
          if (bean != null) {
            listData.add(bean);
          }
        });
      }
    }
    return listData;
  }

  ///json解析为object
  ///[jsonData] json数据
  ///[code] http响应码
  static Future<T?> _parseToObject<T>(String url, Map<String, dynamic>? jsonData, String code) async {
    if (HttpCode.successCodeList.contains(code) && jsonData == null) {
      ///请求成功,响应无data,不用解析
      return null;
    } else {
      return BeanFactory.parseObject<T>(jsonData);
    }
  }
}
