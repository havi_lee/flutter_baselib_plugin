///@date:  2021/2/26 18:09
///@author:  lixu
///@description:接口urls
class HttpUrls {
  HttpUrls._();

  static const releaseHost = 'https://wv.widevision.com.cn/';

  static const httpHost = releaseHost;

  ///登录接口
  static const loginUrl = "login/user/login";

  ///用户列表
  static const userListUrl = "business/user/myDoctor";

  ///聚合接口：演示列表加载和分页逻辑
  ///https://www.juhe.cn/
  static const juheHost = "http://v.juhe.cn/";

  ///聚合-新闻头条
  static const newsUrl = juheHost + "toutiao/index";
}
