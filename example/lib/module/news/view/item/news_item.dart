import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_baselib_example/common/util/ui_utils.dart';
import 'package:flutter_baselib_example/module/news/model/news_bean.dart';

///@date:  2021/10/8 11:48
///@author:  lixu
///@description: 分页加载和下拉刷新功能演示页面List Item
class NewsItem extends StatelessWidget {
  final NewsBean _newsBean;

  NewsItem(this._newsBean);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _getTitleWidget(),
          _getImages(context),
          _getBottomWidget(),
        ],
      ),
    );
  }

  ///标题
  Widget _getTitleWidget() {
    return Padding(
      padding: EdgeInsets.only(
        top: 12,
        bottom: 6,
      ),
      child: Text(
        _newsBean.title ?? 'unknow',
        maxLines: 2,
        style: TextStyle(fontSize: 16, color: Colors.black),
      ),
    );
  }

  ///图片水平列表
  Widget _getImages(context) {
    return Visibility(
      visible: _hasImages(),
      child: Row(
        children: [
          _getImageWidget(context, _newsBean.thumbnailPicS),
          _getImageWidget(context, _newsBean.thumbnailPicS02),
          _getImageWidget(context, _newsBean.thumbnailPicS03),
        ],
      ),
    );
  }

  ///新闻图片
  Widget _getImageWidget(BuildContext context, String? url) {
    if (url == null || url.length == 0) {
      return Divider(
        height: 0,
        color: Colors.transparent,
      );
    } else {
      List<double> sizeList = UIUtils.getImageSize(context);
      return CachedNetworkImage(
        imageUrl: url,
        width: sizeList[0],
        height: sizeList[1],
      );
    }
  }

  ///是否有图片
  bool _hasImages() {
    return (_newsBean.thumbnailPicS != null && _newsBean.thumbnailPicS!.length > 0) ||
        (_newsBean.thumbnailPicS02 != null && _newsBean.thumbnailPicS02!.length > 0) ||
        (_newsBean.thumbnailPicS03 != null && _newsBean.thumbnailPicS03!.length > 0);
  }

  ///获取底部Widget
  Widget _getBottomWidget() {
    return Padding(
      padding: EdgeInsets.only(top: 6, bottom: 12),
      child: Row(
        children: [
          ///作者
          Text(
            _newsBean.authorName ?? '',
            style: TextStyle(
              fontSize: 12,
              color: Colors.grey[500],
            ),
          ),

          ///日期
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Text(
              _newsBean.date ?? '',
              style: TextStyle(
                fontSize: 12,
                color: Colors.grey[500],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
