import 'package:flutter_baselib/src/net/model/http_result_base_bean.dart';

///@date:  2021/2/25 14:22
///@author:  lixu
///@description: http请求失败对象
class HttpErrorBean extends HttpResultBaseBean {
  HttpErrorBean({required String code, String? message}) : super(code: code, message: message);

  @override
  String toString() {
    return '[$code] $message';
  }
}
